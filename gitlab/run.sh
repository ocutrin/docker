#!/bin/bash
docker run --restart always  --privileged \
  -p "8083":"80" -p "443":"443" -p "4567":"4567" -p "8022":"22" \
  -v "/srv/gitlab/config":"/etc/gitlab" \
  -v "/srv/gitlab/data":"/var/opt/gitlab" \
  -v "/srv/gitlab/logs":"/var/log/gitlab" \
  --link postgres-11:postgres \
  --name gitlab ocutrin/gitlab:11.6.5-ce.0
