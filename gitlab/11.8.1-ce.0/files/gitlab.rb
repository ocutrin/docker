# Docker options

# Url
external_url 'http://svrintegracion.settingconsultoria.com:8083'

# Registry
registry_external_url 'http://svrintegracion.settingconsultoria.com:4567'
gitlab_rails['registry_enabled'] = true
gitlab_rails['registry_host'] = "svrintegracion.settingconsultoria.com"
registry['storage_delete_enabled'] = true

## Prevent Postgres from trying to allocate 25% of total memory
postgresql['shared_buffers'] = '1MB'

## Postgresql
postgresql['enable'] = false
gitlab_rails['auto_migrate'] = true
gitlab_rails['db_adapter'] = "postgresql"
gitlab_rails['db_encoding'] = "utf8"
gitlab_rails['db_database'] = "gitlabhq_production"
gitlab_rails['db_username'] = "gitlab"
gitlab_rails['db_password'] = "gitlab"
gitlab_rails['db_host'] = "172.26.0.8"
gitlab_rails['db_port'] = 5432

# Manage accounts with docker
manage_accounts['enable'] = false

# Get hostname from shell
host = `hostname`.strip
external_url "http://#{host}"

# Load custom config from environment variable: GITLAB_OMNIBUS_CONFIG
# Disabling the cop since rubocop considers using eval to be security risk but
# we don't have an easy way out, atleast yet.
eval ENV["GITLAB_OMNIBUS_CONFIG"].to_s # rubocop:disable Security/Eval

# Load configuration stored in /etc/gitlab/gitlab.rb
from_file("/etc/gitlab/gitlab.rb")




