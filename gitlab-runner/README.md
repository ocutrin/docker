# GITLAB-RUNNER
Imagen con gitlab multi-runner instalado.

## Repositorio
`gitlab/gitlab-runner`

## Tags
* v9.5.0

## Variables de entorno
Tenemos una variable para poder crear directorios de despliegues.
* DIRECTORIES

## Volumenes
Tenemos dos volumenes, uno para datos y otro para el socket de docker.
* /etc/gitlab-runner
* /var/run/docker.sock

## Ejecutar contenedor
```
docker run -d \
  --restart=always \
  -e DIRECTORIES="uno dos tres" \
  -v "$VOL1":/etc/gitlab-runner \
  -v "$VOL2":/var/run/docker.sock \
  --name gitlab-runner-container \
  gitlab/gitlab-runner:v9.5.0
```
