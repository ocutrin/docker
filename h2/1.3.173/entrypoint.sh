#!/bin/bash

if [ "$(id -u)" = '0' ]; then
  chown ${USUARIO}:${USUARIO} ${H2_DATA}
  chmod 755 ${H2_DATA}
  exec gosu "${USUARIO}" "${BASH_SOURCE}" "$@"
fi

if [ "$(id -u)" = '1000' ]; then
  exec java -cp /opt/h2/bin/h2*.jar org.h2.tools.Server \
   	-web -webAllowOthers -webPort 8082 \
   	-tcp -tcpAllowOthers -tcpPort 9092 \
   	-baseDir ${H2_DATA}
fi
