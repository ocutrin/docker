#!/bin/bash

set -e

if [ "$(id -u)" = '0' ]; then
 
  if [[ -z $SONARQUBE_JDBC_USERNAME ]]; then
    exec echo "Error: no se ha seteado un usuario sonar de la bd."
  fi
 
  if [[ -z $SONARQUBE_JDBC_PASSWORD ]]; then
    exec echo "Error: no se ha seteado una password para el usuario sonar de la bd."
  fi
 
  if [[ -z $SONARQUBE_JDBC_URL ]]; then
    exec echo "Error: no se ha seteado una url de la bd."
  fi
 
  chown sonar:sonar /opt/sonarqube -R
  chmod 775 /opt/sonarqube -R

  exec gosu sonar $BASH_SOURCE $@

fi

java -jar /opt/sonarqube/lib/sonar-application-$SONAR_VERSION.jar \
  -Dsonar.log.console="true" \
  -Dsonar.jdbc.username="$SONARQUBE_JDBC_USERNAME" \
  -Dsonar.jdbc.password="$SONARQUBE_JDBC_PASSWORD" \
  -Dsonar.jdbc.url="$SONARQUBE_JDBC_URL" \
  -Dsonar.web.javaAdditionalOpts="$SONARQUBE_WEB_JVM_OPTS -Djava.security.egd=file:/dev/./urandom"

