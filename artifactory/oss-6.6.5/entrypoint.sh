#!/bin/bash

#if [ "$(id -u)" = '0' ]; then

  ok=true

  if [[ -z $ARTIFACTORY_JDBC_USERNAME ]]; then
    echo "INFO: no se ha seteado un usuario artifactory"
    ok=false
  fi
  if [[ -z $ARTIFACTORY_JDBC_PASSWORD ]]; then
    echo "INFO: no se ha seteado una password para el usuario artifactory"
    ok=false
  fi
  if [[ -z $ARTIFACTORY_JDBC_URL ]]; then
    echo "INFO: no se ha seteado una url de base de datos"
    ok=false
  fi

  if [[ ! ok ]]; then
    echo "INFO: ALguna de las variables necesarias no ne han seteado con lo que \
      artifactory no tendra acceso a la base de datos externa."
  fi

  # if [[ ! -f $ARTIFACTORY_HOME/etc/db.properties ]]; then
    # sed -i "s#url=jdbc:postgresql://localhost:5432/artifactory#url=$ARTIFACTORY_JDBC_URL#g" $ARTIFACTORY_HOME/misc/db/postgresql.properties
    # sed -i "s#username=artifactory#username=$ARTIFACTORY_JDBC_USERNAME#g" $ARTIFACTORY_HOME/misc/db/postgresql.properties
    # sed -i "s#password=password#password=$ARTIFACTORY_JDBC_PASSWORD#g" $ARTIFACTORY_HOME/misc/db/postgresql.properties
    # cp $ARTIFACTORY_HOME/misc/db/postgresql.properties $ARTIFACTORY_HOME/etc/db.properties
  # fi

  #$ARTIFACTORY_HOME/bin/installService.sh

  #exec gosu artifactory $BASH_SOURCE $@

#fi

$ARTIFACTORY_HOME/bin/artifactory.sh

exec $@
