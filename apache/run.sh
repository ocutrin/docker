#!/bin/bash

docker volume create \
  -d local-persist \
  -o mountpoint=/srv/apache

docker run \
  --restart always \
  -p 8282:80 \
  -v apache:/var/www/html \
  --link mysql8:mysql8 \
  --link mysql5:mysql5 \
  --name apache \
  -d \
  ocutrin/apache:a2p5

