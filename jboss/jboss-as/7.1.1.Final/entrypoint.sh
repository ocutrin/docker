#!/bin/bash
set -e

# Cuando entramos como root para la configuracion final
if [ "$(id -u)" = '0' ]; then

  echo "Configurando permisos..."

  #Añadimos usuario adminstrador para la consola web
  if [[ ! -z $JBOSSAS_USER ]] && [[ ! -z $JBOSSAS_PASSWORD ]]; then
    echo "Configurando usuario adminstrador..."
    $JBOSS_HOME/bin/add-user.sh --silent=true "$JBOSSAS_USER" "$JBOSSAS_PASSWORD" > /tmp/capture.log 2>&1
  fi

  # Generamos y configuramos SSL
  echo "Configurando SSL..."
  if [[ -z $SSL_STOREPASS ]]; then
    SSL_STOREPASS="password"
  fi
  if [[ -z $SSL_DNAME ]]; then
    SSL_DNAME="CN=NombreComun"
  fi
  if [[ -z $SSL_VALIDITY ]]; then
    SSL_VALIDITY="10950"
  fi
  if [[ -z $SSL_KEYSTORE ]]; then
    SSL_KEYSTORE="application.keystore"
  fi
  if [[ -z $SSL_KEYPASS ]]; then
    SSL_KEYPASS="password"
  fi

  keytool -genkey -alias server -noprompt -trustcacerts -dname "$SSL_DNAME" \
    -keyalg RSA -keystore "$SSL_KEYSTORE" -validity "$SSL_VALIDITY" \
    -storepass "$SSL_STOREPASS" -keypass "$SSL_KEYPASS"

  keytool -list -v -keystore "$SSL_KEYSTORE" -storepass "$SSL_STOREPASS"

  mv "$SSL_KEYSTORE" $JBOSS_HOME/standalone/configuration/

fi

echo "Iniciando jboss-as..."
exec $JBOSS_HOME/bin/standalone.sh -b 0.0.0.0 -bmanagement 0.0.0.0
