# JBOSS
Imagen con jboss.

# Repositorio
`svrintegracion.settingconsultoria.com:4567/docker/repositorios/jboss/jboss-as`

## Tags
- [7.1.1.Final](7.1.1.Final)
- [latest](1.0)

## Variables entorno
Tenemos las siguientes variables de entorno.
* JBOSSAS_USER
* JBOSSAS_PASSWORD
* CONSOLE
* DEBUG

## Puertos
Tenemos tres puertos disponibles, uno para cliente web, otro para la consola de administracion y otro para debug.
* 8080
* 9990
* 8787

## Volumenes
Tenemos tres volumenes, uno para despliegues y otro para el log.
* /opt/jboss/jboss-as/standalone/deployments
* /opt/jboss/jboss-as/standalone/configuracion/log

## Ejecutar contenedor
```
docker run \
  -e WILDFLY_USER=admin \
  -e WILDFLY_PASSWORD=admin \
  -e CONSOLE=true \
  -e DEBUG=false \
  -p 8080:8080 \
  -p 9990:9990 \
  -p 8787:8787 \
  -v /srv/jboss/jboss-as/deployments:/opt/jboss/jboss-ass/standalone/deployments \
  -v /srv/jboss/jboss-as/log:/opt/jboss/jboss-as/standalone/log \
  --link postgres-container:postgres \
  --link h2-container:h2 \
  --name jboss-as-container \
  svrintegracion.settingconsultoria.com:4567/docker/repositorios/jboss/jboss-as
```

## Docker-compose
```
jbossas:
  image: svrintegracion.settingconsultoria.com:4567/docker/repositorios/jboss/jboss-as
  container_name: jboss-as-container
  environment:
    - JBOSSAS_USER=admin
    - JBOSSAS_PASSWORD=admin
    - CONSOLE=true
    - DEBUG=false
  ports:
    - "8080:8080"
    - "9990:9990"
    - "8787:8787"
  links:
    - postgres:postgres
    - h2:h2
  volumes:
    - /srv/jboss/jboss-as/deployments:/opt/jboss/jboss-as/standalone/deployments
    - /srv/jboss/jboss-as/log:/opt/jboss/jboss-as/standalone/log
```
