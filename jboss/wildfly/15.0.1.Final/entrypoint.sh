#!/bin/bash

set -e

if [ "$(id -u)" = '0' ]; then

  ssl_dir=$JBOSS_HOME/standalone/configuration

  if [ ! -f "$ssl_dir/$SSL_KEYSTORE" ]; then

    if [[ ! -z $WILDFLY_USER ]] && [[ ! -z $WILDFLY_PASSWORD ]]; then
      $JBOSS_HOME/bin/add-user.sh $WILDFLY_USER $WILDFLY_PASSWORD --silent
    fi

    if [[ -z $SSL_STOREPASS ]]; then
      SSL_STOREPASS="password"
    fi

    if [[ -z $SSL_DNAME ]]; then
      SSL_DNAME="CN=NombreComun"
    fi

    if [[ -z $SSL_VALIDITY ]]; then
      SSL_VALIDITY="10950"
    fi

    if [[ -z $SSL_KEYSTORE ]]; then
      SSL_KEYSTORE="application.keystore"
    fi

    if [[ -z $SSL_KEYPASS ]]; then
      SSL_KEYPASS="password"
    fi

    keytool -genkey -alias server -noprompt -trustcacerts -dname $SSL_DNAME \
      -keyalg RSA -keystore $SSL_KEYSTORE -validity $SSL_VALIDITY \
      -storepass $SSL_STOREPASS -keypass $SSL_KEYPASS
    keytool -list -v -keystore $SSL_KEYSTORE -storepass $SSL_STOREPASS

    mv $SSL_KEYSTORE $ssl_dir/

  fi

  exec gosu jboss $BASH_SOURCE $@

fi

echo "Iniciando wildfly..."
exec $JBOSS_HOME/bin/standalone.sh -b 0.0.0.0 -bmanagement 0.0.0.0
