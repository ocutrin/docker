#!/bin/bash

docker volume create \
  -d local-persist \
  -o mountpoint=/srv/postgres/11 \
  --name=postgres11

docker run \
  --name postgres11\
  -p 5432:5432 \
  -e POSTGRES_USER="admin" \
  -e POSTGRES_PASSWORD="admin" \
  -v postgres11:/var/lib/postgresql/data \
  -d postgres:11.1
