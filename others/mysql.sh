#!/bin/bash

docker run \
  --name mysql5 \
  -e MYSQL_ROOT_PASSWORD="admin" \
  -p 3306:3306 \
  -d \
  mysql:5.7.24

docker run \
  --name mysql8 \
  -e MYSQL_ROOT_PASSWORD="admin" \
  -p 3307:3306 \
  -d \
  mysql:8.0.13
