#!/bin/bash

docker run \
  -e 'ACCEPT_EULA=Y' \
  -e 'SA_PASSWORD=Admin0Admin0' \
  -p 1433:1433 \
  --name sqlserver2017 \
  --net red-integracion \
  --ip 172.13.1.24 \
  -d \
  mcr.microsoft.com/mssql/server:2017-latest

docker run \
  -e 'ACCEPT_EULA=Y' \
  -e 'SA_PASSWORD=Admin0Admin0' \
  -p 1434:1433 \
  --name sqlserver2019 \
  --net red-integracion \
  --ip 172.13.1.25 \
  -d \
  mcr.microsoft.com/mssql/server:2019-CTP2.2-ubuntu

