#!/bin/bash

docker volume create \
  -d local-persist \
  -o mountpoint="/srv/pgadmin/4/" \
  --name pgadmin4

docker run \
  --name pgadmin4 \
  -p 8081:80 \
  -v "pgadmin4:/var/lib/pgadmin" \
  -e PGADMIN_DEFAULT_EMAIL="admin" \
  -e PGADMIN_DEFAULT_PASSWORD="admin" \
  -d dpage/pgadmin4:4.1
