#!/bin/bash
set -e

if [[ "$(id -u)" = "0" ]];then

    # Usuario admin.
    ADMIN_USER="admin"
    if [[ -z $ADMIN_PASSWORD ]]; then
        ADMIN_PASSWORD="admin"
    fi
    ADMIN_GROUP=${ADMIN_USER}

    # Ususario anonimo.
    ANON_USER="anon"
    if [[ -z $ANON_PASSWORD ]]; then
        ANON_PASSWORD="anon"
    fi
    ANON_GROUP=${ANON_USER}

    # Ususario test.
    TEST_USER="test"
    if [[ -z $TEST_PASSWORD ]]; then
        TEST_PASSWORD="test"
    fi
    TEST_GROUP=${TEST_USER}

    # Grupo usuarios sftp.
    SFTP_USERS_GROUP=sftpusers

    # Añadimos grupos necesarios
    groupadd $ADMIN_GROUP
    groupadd $ANON_GROUP
    groupadd $TEST_GROUP
    groupadd $SFTP_USERS_GROUP

    # Directorio HOME ususarios sftp.
    HOME=/home
    TEST_CHROOT_DIR=$HOME/$TEST_USER
    ANON_CHROOT_DIR=/var/sftp

    # Asignamos permisos y dueños.
    mkdir -p $ANON_CHROOT_DIR $TEST_CHROOT_DIR
    chown root:root $ANON_CHROOT_DIR $TEST_CHROOT_DIR
    chmod 755 $ANON_CHROOT_DIR $TEST_CHROOT_DIR

    # Creamos los usuarios.
    useradd -g $ADMIN_GROUP -m -s /bin/bash $ADMIN_USER
    echo $ADMIN_USER:$ADMIN_PASSWORD | chpasswd
    useradd -g $ANON_GROUP -G $SFTP_USERS_GROUP -d $ANON_CHROOT_DIR $ANON_USER
    echo $ANON_USER:$ANON_PASSWORD | chpasswd
    useradd -g $TEST_GROUP -G $SFTP_USERS_GROUP -d $TEST_CHROOT_DIR $TEST_USER
    echo $TEST_USER:$TEST_PASSWORD | chpasswd

    # Comprobamos directorio de privilegios separados sino lo creaamos
    if [[ ! -d /var/run/sshd ]]; then
    echo "Creando directorio de privilegios..."
    mkdir /var/run/sshd
    chmod 777 /var/run
    chmod 755 /var/run/sshd
    fi

    # Creamos directorios.
    mkdir -p  $DATA_DIR $TEST_CHROOT_DIR/test
    chown $ADMIN_USER:$ANON_GROUP $DATA_DIR
    chown $ADMIN_USER:$TEST_GROUP $TEST_CHROOT_DIR/test
    chmod 750 $DATA_DIR
    chmod 770 $TEST_CHROOT_DIR/test

    # Borramos y escribimos nuestro mensaje en el archivo de configuracion ssh.
    cat /dev/null > /etc/issue.net
    echo -e "Servidor sftp docker container\nMAINTAINER: ocutrin" >> /etc/issue.net

    # Clave SSH
    mkdir $HOME/$ADMIN_USER/.ssh
    mkdir $HOME/$TEST_USER/.ssh

    chmod 755 $HOME/$ADMIN_USER/.ssh
    chmod 755 $HOME/$TEST_USER/.ssh

    mkdir $ANON_CHROOT_DIR/.ssh
    chmod 755 $ANON_CHROOT_DIR/.ssh

    rm /etc/ssh/ssh_host*
    ssh-keygen -f /etc/ssh/sftp_rsa_key -t rsa -N ''
    cat /etc/ssh/sftp_rsa_key.pub >> /etc/ssh/authorized_keys
    chown $USER:$USER /etc/ssh/sftp_rsa_key
    chmod 755 /etc/ssh/sftp_rsa_key

    cp /etc/ssh/authorized_keys $HOME/$ADMIN_USER/.ssh/
    cp /etc/ssh/sftp_rsa_key $HOME/$ADMIN_USER/.ssh/

    cp /etc/ssh/authorized_keys $HOME/$TEST_USER/.ssh/
    cp /etc/ssh/sftp_rsa_key $HOME/$TEST_USER/.ssh/

    cp /etc/ssh/authorized_keys $ANON_CHROOT_DIR/.ssh/
    cp /etc/ssh/sftp_rsa_key $ANON_CHROOT_DIR/.ssh/

    exec gosu sftp $BASH_SOURCE $@

fi

# Info
echo "Ejecute este comando en el equipo host para copiar la clave de acceso SSH."
echo -e "\e[33mdocker cp sftp-container:/etc/ssh/ssh_host_rsa_key ./\e[0m"
echo "Iniciando servidor SSH y habilitando servidor sftp..."

# Ejecutamos argumentos.
exec "$@"
