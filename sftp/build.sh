#!/bin/bash

docker build -t ocutrin/sftp:1.0 1.0

if [[ ! -z $1 ]]; then
    docker build -t ocutrin/sftp:latest $1
fi
